<?php

/**
 * @file
 *   Contains the page callback functions for this module.
 */

/**
 * Form constructor for inviting members into groups.
 */
function og_invite_link_invite_page_form($form, $form_state = array(), $entity_type, $etid) {
  // Set the page title (needed because it's a local task)
  drupal_set_title(t('Invite members to this group'), PASS_THROUGH);
  og_set_breadcrumb($entity_type, $etid, array(l(t('Group'), "$entity_type/$etid/group")));

  $form = array();
  // Form is only ever called from the menu now (it could have been from an
  // action, but we've removed this.)
  if ($group = og_get_group($entity_type, $etid)) {
    $groups = array($group);
  }
  else {
    // This entity isn't a group.
    return MENU_NOT_FOUND;
  }
  $form['groups'] = array(
    '#type' => 'value',
    '#value' => $groups,
  );
  $form['invitees'] = array(
    '#type' => 'textfield',
    '#title' => t('Invitees'),
    '#maxlength' => 1024,
    '#required' => FALSE,
    '#autocomplete_path' => 'og_invite_link/autocomplete',
    '#description' => t('Add one or more usernames in order to invite & add users to this group. Multiple usernames should be separated by a comma.'),
  );
  if ($max = variable_get('og_invite_link_max_invites_per_form')) {
    $form['invitees']['#description'] .= t(' A maximum of !max invites can be sent out at a time.', array('!max' => $max));
  }
  $form['emails'] = array(
    '#type' => 'textarea',
    '#title' => t('Invite by email'),
    '#rows' => 20,
    '#required' => FALSE,
    '#description' => t('Add one or more email addresses in order to invite users in this group. Place one email address on each line.'),
    '#access' => variable_get('user_register', TRUE),
  );
  if ($max = variable_get('og_invite_link_max_invites_per_form')) {
    $form['emails']['#description'] .= t(' A maximum of !max invites can be sent out at a time.', array('!max' => $max));
  }
  $form['additional_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Additional message'),
    '#description' => t('Insert an additional message to be sent to the users.')
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Invite'),
  );
  return $form;
}

/**
 * Validation handler for the invite form.
 */
function og_invite_link_invite_page_form_validate($form, &$form_state) {
  $invitees = array();

  // Extract the user names to send invites to
  $names = explode(',', $form_state['values']['invitees']);

  // Extract the emails to send invites to
  $emails = explode("\n", $form_state['values']['emails']);

  // Add them to an array, filtering out whitespace entries
  // in order to get a real count
  foreach ($names as $name) {
    $name = trim($name);
    if ($name) {
      $invitees[] = trim($name);
    }
  }
  foreach ($emails as $email) {
    $email = trim($email);
    if (!empty($email)) {
      $email = trim($email);
      if (valid_email_address($email)) {
        $invitees[] = $email;
      }
      else {
        form_set_error('emails', t('@email is an invalid email address.', array('@email' => $email)));
      }
    }
  }

  // Make sure we have no more than the max amount of invites
  // @todo check also the number of groups.
  $max = variable_get('og_invite_link_max_invites_per_form', 200);
  if (!empty($max) && count($invitees) > $max) {
    form_set_error('invitees', t('You can only send !max invitations at a time', array('!max' => $max)));
  }
}

/**
 * Submit handler for the invite form.
 */
function og_invite_link_invite_page_form_submit($form, &$form_state) {
  $invitation_result = og_invite_link_invite_users_to_groups(array(
                                          'groups' => $form_state['values']['groups'],
                                          'invitees' => $form_state['values']['invitees'],
                                          'emails' => $form_state['values']['emails'],
                                          'additional_message' => $form_state['values']['additional_message']));
  // Here we should not have more than one group.
  $group = current($form_state['values']['groups']);
  // Set a message for the invited users
  if (!empty($invitation_result['invitees'][$group->gid])) {
    drupal_set_message(t('An invite has been sent to !users', array('!users' => implode(', ', $invitation_result['invitees'][$group->gid]))));
  }

  // Set a message for the invitees already in the group
  if (!empty($invitation_result['in_group'][$group->gid])) {
    drupal_set_message(t('The following invitees are already members of the group: !users', array('!users' => implode(', ', $invitation_result['in_group'][$group->gid]))), 'warning');
  }

  // Set a message for the invalid invitees
  if (!empty($invitation_result['invalid'])) {
    drupal_set_message(t('The following invitees are invalid users or are blocked: !users', array('!users' => implode(', ', $invitation_result['invalid']))), 'error');
  }
}

/**
 * Helper function to set message & redirect in the case of invalid invitations.
 *
 * @param $msg
 *   Message. Default: 'This is not a valid invitation.'
 * @param
 *   Any additional arguments are passed to drupal_goto().
 *
 * @return
 *   Returns MENU_NOT_FOUND.
 */
function _og_invite_link_invalid_invitation($msg = NULL) {
  $args = func_get_args();
  array_shift($args);
  if (empty($msg)) {
    $msg = t('This is not a valid invitation.');
  }
  if (empty($args[0])) {
    $dest = '<front>';
  }
  drupal_set_message($msg, 'error');
  call_user_func_array('drupal_goto', $args);
  return MENU_NOT_FOUND;
}

/**
 * Page callback for joining a group.
 *
 * @param string $entity_type
 *   The group's entity type.
 * @param int $etid
 *   The group's entity ID. (e.g. node ID)
 * @param object $account
 *   The account in the invitation link
 * @param string $token
 *   The invitation token
 */
function og_invite_link_join($entity_type, $etid, $account, $token) {
  global $user;
  if (is_numeric($account)) {
    $account = user_load($account);
    if ($account == FALSE) {
      return _og_invite_link_invalid_invitation();
    }
  }
  elseif (is_string($account) && valid_email_address($account)) {
    $loaded = user_load_by_mail($account);
    $account = $loaded ? $loaded : $account;
  }
  else {
    return _og_invite_link_invalid_invitation();
  }

  // Load the invitation from the token
  $invitation = og_invite_link_get_invitation_by_token($token);

  // Determine if this is a valid invitation
  if ($invitation == FALSE) {
    return _og_invite_link_invalid_invitation();
  }
  // If the user is logged in, make sure the invitation is meant for them.
  // We will be logging the user in later otherwise (if the invitation was for
  // an already-registered user).
  elseif (!empty($user->uid)) {
    if (empty($user->mail) || ($user->uid != $invitation->uid && $user->mail != $invitation->uid)) {
      return _og_invite_link_invalid_invitation();
    }
  }

  // Make sure the invitation is meant for this group
  $group = og_get_group($entity_type, $etid);
  if ($group->gid != $invitation->gid) {
    return _og_invite_link_invalid_invitation();
  }

  // Make sure this invitation was intended for the account included in the
  // invitation link. Note that $account has come from the URL.
  // First, if $account is an string, check if its a different email to our
  // logged-in user. Secondly, if $account is an object, check if the invitation
  // is for this user's uid or email address. Finally, check if $account is
  // anything else.
  $valid = TRUE;
  if (is_numeric($invitation->uid)) {
    $valid = (is_object($account) && $account->uid == $invitation->uid);
  }
  elseif (is_string($invitation->uid)) {
    if (is_object($account)) {
      $valid = ($account->mail == $invitation->uid);
    }
    elseif (is_string($account)) {
      $valid = ($account == $invitation->uid);
    }
    else {
      $valid = FALSE;
    }
  }
  else {
    $valid = FALSE;
  }
  if ($valid == FALSE) {
    return _og_invite_link_invalid_invitation();
  }

  // See if the invitation has expired
  if ($invitation->accepted_timestamp) {
    return _og_invite_link_invalid_invitation(t('This invitation has expired.'));
  }

  // Log the user in if not yet logged in.
  if (!$user->uid) {
    // Make sure the invited user is active and exists.
    if (is_object($account)) {
      if ($account->uid && $account->status) {
        // Log the user in
        $user = $account;
        user_login_finalize();
      }
      else {
        return _og_invite_link_invalid_invitation();
      }
    }
    elseif (variable_get('user_register', TRUE)) {
      // We will need a new account for the user with the email held by $account
      return _og_invite_link_invalid_invitation(t('Please register to join the campaign.'), 'user/register', array('query' => drupal_get_destination()));
    }
    else {
      return _og_invite_link_invalid_invitation();
    }
  }

  // If we don't have a logged in user by now, the invitation is invalid
  // We probably don't need this check, but hey, what's the harm? :-)
  if (!$user->uid) {
    return _og_invite_link_invalid_invitation();
  }

  // Get the group's base entity as we'll need it for redirecting.
  $entity = og_load_entity_from_group($group->gid);

  $account = clone $user;
  // See if the user is already in the group
  if (og_is_member($invitation->gid)) {
    drupal_set_message(t('You are already a member of this group.'));
    // Remove any pending invitations for this user and this group
    //db_query("DELETE FROM {og_invite_link} WHERE uid = %d AND gid = %d AND accepted_timestamp = 0", $invitation->uid, $invitation->gid, $invitation->token);
    db_delete('og_invite_link')
      ->condition(db_or()->condition('uid', $account->uid)->condition('uid', $account->mail))
      ->condition('gid', $invitation->gid)
      ->condition('accepted_timestamp', 0)
      ->execute();
  }
  else {
    // Determine the moderation status of the invitation
    $state = og_user_access_by_entity('subscribe without approval', $group->entity_type, $group->etid, $account) ? OG_STATE_ACTIVE : FALSE;
    if ($state == FALSE) {
      $sender = user_load($invitation->sender);
      // Approved if the inviter is allowed to just add users,
      // or if the invitee can subscribe & the inviter can approve this.
      // Otherwise (invitee can't subscribe & just be added, or can subscribe
      // but can't be approved), it's an invalid invitation. In these cases,
      // show the group page.
      if (og_user_access_by_entity('add user', $group->entity_type, $group->etid, $sender)) {
        $state = OG_STATE_ACTIVE;
      }
      elseif (og_user_access_by_entity('subscribe', $group->entity_type, $group->etid, $account)) {
        $state = og_user_access_by_entity('approve and deny subscription', $group->entity_type, $group->etid, $sender) ? OG_STATE_ACTIVE : OG_STATE_PENDING;
      }
      else {
        // If moderated and the group is now closed, this is no
        // longer a valid invitation. Alert the user.
        $uri = entity_uri($group->entity_type, $entity);
        return _og_invite_link_invalid_invitation(NULL, $uri['path'], $uri['options']);
      }
    }

    // Update the invitation to mark it as accepted.
    db_update('og_invite_link')
      ->fields(array(
        'accepted_timestamp' => REQUEST_TIME
      ))
      ->condition('token', $invitation->token)
      ->execute();

    // Remove any other pending invitations for this user and group/token.
    db_delete('og_invite_link')
      ->condition(db_or()->condition('uid', $account->uid)->condition('uid', $account->mail))
      ->condition('gid', $invitation->gid)
      ->condition('token', $invitation->token, '<>')
      ->execute();

    // Create a group subscription for the user:
    og_group($invitation->gid, array('entity type' => 'user', 'entity' => $account, 'state' => $state));

    // Set a message based on the moderation status
    if ($state == OG_STATE_PENDING) {
      drupal_set_message(t('You have requested access to join the group. A group administrator must first approve your request before you can join the group.'), 'warning');
    }
    else {
      drupal_set_message(t('You are a member of this group now.'));
    }
  }

  $uri = entity_uri($group->entity_type, $entity);
  drupal_goto($uri['path'], $uri['options']);
}

/**
 * Helper function for autocompletion.
 * @see taxonomy_autocomplete()
 *
 * @todo A good thing would be to avoid returning
 *   usernames that are already members in the group.
 *   For that, the group id has to be sent as a parameter.
 */
function og_invite_link_autocomplete($string = '') {
  // The user enters a comma-separated list of names. We only autocomplete the last name.
  $array = drupal_explode_tags($string);

  // Fetch last name
  $last_string = trim(array_pop($array));
  $matches = array();
  if ($last_string != '') {
    //$result = db_query_range("SELECT name FROM {users} WHERE LOWER(name) LIKE LOWER('%s%%')", $last_string, 0, 10);
    $result = db_select('users')->fields('users', array('name'))->condition('name', db_like($last_string) . '%', 'LIKE')->range(0, 10)->execute();

    $prefix = count($array) ? implode(', ', $array) . ', ' : '';

    foreach ($result as $name) {
      $n = $name->name;
      // Commas and quotes in usernames are special cases, so encode 'em.
      //(Although the usernames should not have such characters).
      if (strpos($name->name, ',') !== FALSE || strpos($name->name, '"') !== FALSE) {
        $n = '"' . str_replace('"', '""', $name->name) . '"';
      }
      $matches[$prefix . $n] = check_plain($name->name);
    }
  }

  drupal_json_output($matches);
}

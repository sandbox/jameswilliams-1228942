<?php

/**
 * @file
 *  Contains administration pages.
 */

/**
 * Settings form for this module.
 *
 */
function og_invite_link_admin() {
  $form = array();

  $form['og_invite_link_max_invites_per_form'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum invites at a time'),
    '#description' => t('Set the number of invites that can be sent at any one time. For no limit, set to zero or leave blank.'),
    '#default_value' => variable_get('og_invite_link_max_invites_per_form'),
    '#element_validate' => array('_element_validate_integer'),
  );

  //Provide an option for site administrators to choose
  //how long the invitations are valid. Expired ones are
  //just purged at cron.
  $options = array(1 => 1, 2 => 2, 5 => 5, 10 => 10, 15 => 15, 30 => 30, 45 => 45, 60 => 60, 90 => 90, 0 => t('Never'));
  $form['og_invite_link_expiration'] = array(
    '#type' => 'select',
    '#title' => t('Valid for number of days'),
    '#description' => t('Select the number of days after which the invitations will become expired.'),
    '#default_value' => variable_get('og_invite_link_expiration', 30),
    '#options' => $options,
  );

  $form['og_settings']['notifications']['og_invite_link_user_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Invite user notification subject'),
    '#description' => t('The subject of the message sent to users invited to join a group. Available variables: @group, @site, !group_url, @body, @sent-to, @sent-by.'),
    '#default_value' => _og_invite_link_mail_text('og_invite_link_user_subject'),
    '#weight' => 15,
  );
  $form['og_settings']['notifications']['og_invite_link_user_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Invite user notification body'),
    '#rows' => 10,
    '#description' => t('The body of the message sent to users invited to join a group. Available variables: @group, @site, !group_url, @body, @sent-to, @sent-by.'),
    '#default_value' =>  _og_invite_link_mail_text('og_invite_link_user_body'),
    '#weight' => 16,
  );

  $form['og_settings']['notifications']['og_invite_link_email_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Invite user by email notification subject'),
    '#description' => t('The subject of the message sent to people that are not yet site members invited to join a group. Available variables: @group, @site, !group_url, @body, @sent-to, @sent-by.'),
    '#default_value' => _og_invite_link_mail_text('og_invite_link_email_subject'),
    '#weight' => 17,
  );
  $form['og_settings']['notifications']['og_invite_link_email_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Invite user by email notification body'),
    '#rows' => 10,
    '#description' => t('The body of the message sent to people that are not yet site members invited to join a group. Available variables: @group, @site, !group_url, @body, @sent-to, @sent-by.'),
    '#default_value' => _og_invite_link_mail_text('og_invite_link_email_body'),
    '#weight' => 18,
  );

  /*// Add a message template for notifying group admins of sent invitations
  $form['og_settings']['notifications']['og_invite_link_admin_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Group admin invite notification subject'),
    '#description' => t('The subject of the message sent to group admins whenever a user sends out a group invitation. Available variables: @group, @site, !group_url, @body, @sent-to, @sent-by.'),
    '#default_value' => _og_invite_link_mail_text('og_invite_link_admin_subject'),
    '#weight' => 19,
  );
  $form['og_settings']['notifications']['og_invite_link_admin_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Group admin invite notification body'),
    '#description' => t('The body of the message sent to group admins whenever a user sends out a group invitation. Available variables: @group, @site, !group_url, @body, @sent-to, @sent-by.'),
    '#default_value' => _og_invite_link_mail_text('og_invite_link_admin_body'),
    '#weight' => 20,
  );*/

  return system_settings_form($form);
}
